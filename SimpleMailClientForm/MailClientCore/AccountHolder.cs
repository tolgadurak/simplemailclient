﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleMailClientForm.MailClientCore
{
    public class AccountHolder
     {
        public String Username { set; get; }
        public String Password { set; get; }
        public String IMAPServerName { private set; get; }
        public String SMTPServerName { private set; get; }
        public bool Ssl { private set; get; }

        public const String GmailIMAPServer = "imap.gmail.com";
        public const String OutlookIMAPServer = "imap-mail.outlook.com";
        public int IMAPPort { private set; get; }
        public int SMTPPort { private set; get; }

        public
         AccountHolder(String username, String password)
        {
            Username = username;
            Password = password;
            InitializeServerSettings();
            String err = InitializeAccountSettings(username, password) ? "Valid E-mail" : "Invalid E-mail";
            Console.WriteLine(err);
       }

        public AccountHolder()
        {
          
        }
        
        public static AccountHolder CreateAccount(String username, String password)
        {
            return new AccountHolder(username, password);
        }

        public void InitializeServerSettings()
        {
            if (!MathesRegex(Username, Password)) return;

            string controlServer = Username.Substring(Username.IndexOf('@'));
            if (controlServer == "@gmail.com")
            {
                Ssl = true;
                IMAPPort = 993;
                SMTPPort = 587;
                IMAPServerName = "imap.gmail.com";
                SMTPServerName = "smtp.gmail.com";
            }
            else if (controlServer == "@hotmail.com" 
                     || controlServer == "@windowslive.com" 
                     || controlServer == "@live.com" 
                     || controlServer == "@msn.com")
            {
                Ssl = true;
                IMAPPort = 993;
                SMTPPort = 587;
                SMTPServerName = "smtp-mail.outlook.com";
                IMAPServerName = "imap-mail.outlook.com";
            }
        }

        private bool InitializeAccountSettings(String username, String password)
        {
            Regex pattern = new Regex("\\w+@\\w+\\.{1}\\w+");           
            Match match = pattern.Match(username);
            if (match.Success)
            {
                this.Username = username;
                this.Password = password;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsAccountNull()
        {
            return (IMAPServerName == null || Password == null || SMTPServerName == null || IMAPPort ==0 || SMTPPort == 0) ? true : false;
        }
        private bool MathesRegex(String username, String password)
        {
            Regex pattern = new Regex("\\w+@\\w+\\.{1}\\w+");
            Regex passPattern = new Regex("\\w+");
            Match match = pattern.Match(username);
            Match passMatch = passPattern.Match(password);
            return match.Success && passMatch.Success;
        }

        public void Clear()
        {
            Username = "";
            Password = "";
            IMAPServerName = "";
            SMTPServerName = "";
            IMAPPort = 0;
            SMTPPort = 0;
            Ssl = false;
        }

    }
}
