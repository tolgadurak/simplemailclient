﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImapX;
using System.Net.Mail;
using ImapX.Constants;
namespace SimpleMailClientForm.MailClientCore
{
    public class IMAPServer {
     ImapClient client;
     AccountHolder account;
        public Folder Inbox { private set; get; }
        public Folder Sent { private set; get; }
        public Folder Drafts { private set; get; }
        public Folder Trash { private set; get; }


        public IMAPServer(AccountHolder account)
        {
            InitializeServer(account);
        }
        public IMAPServer()
        {
            
        }
        public bool Logout()
        {
           return client.Logout();
        }
        public IEnumerable<Message> GetAllMessagesFromSpecifiedFolder(Folder folder)
        {
            folder.Messages.Download();

            return folder.Messages;
        }

        public bool EnableAutoSync(Folder folder)
        {
            folder.OnNewMessagesArrived += Folder_OnNewMessagesArrived;
            return folder.StartIdling();
        }

        public void Folder_OnNewMessagesArrived(object sender, IdleEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("new message arrived");
            Inbox.Messages.Download();
            MailClientForm.inbox.Update();
        }

        public void InitializeServer(AccountHolder account) {
           client = new ImapClient(account.IMAPServerName, account.IMAPPort, account.Ssl);
            if (client.Connect())
            {
                if (client.Login(account.Username, account.Password))
                {                             
                    Inbox = client.Folders.Inbox;                   
                    Sent = client.Folders.Sent;
                    Drafts = client.Folders.Drafts;
                    Trash = client.Folders.Trash;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Hatalı kullanıcı adı ya da şifre");
                    account.Clear();
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Server failed");
                account.Clear();
            }
        
        }
    }
}
