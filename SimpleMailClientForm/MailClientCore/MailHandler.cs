﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace SimpleMailClientForm.MailClientCore
{
    public class MailHandler
    {
        public static MailMessage Mail = new MailMessage();

        public static void CreateNewMail(String sender, String receiver, String subject, String body) {
            Mail.From = new MailAddress(sender);
            Mail.To.Add(receiver);
            Mail.Subject = subject;
            Mail.Body = body;
            
        }
        public static void CreateNewMail(String sender, String receiver, String subject, String body, Attachment attachment)
        {
            CreateNewMail(sender, receiver, subject, body);
            Mail.Attachments.Add(attachment);
        }

        public static void AddAttachment(Attachment attachment)
        {
            Mail.Attachments.Add(attachment);
        }

    }
}
