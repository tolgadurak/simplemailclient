﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
namespace SimpleMailClientForm.MailClientCore
{
   public class SmtpServer
    {
         SmtpClient server;
         AccountHolder mailAccount;

        public SmtpServer(AccountHolder mailAccount)
        {
            InitializeServer(mailAccount);
        }
       public SmtpServer()
        {

        }
       public void InitializeServer(AccountHolder mailAccount)
       {
           this.mailAccount = mailAccount;
           if (mailAccount.IsAccountNull())
               return;
           server = new SmtpClient();
           server.EnableSsl = mailAccount.Ssl;
           server.Port = mailAccount.SMTPPort;
           server.Host = mailAccount.SMTPServerName;
           server.Credentials = new System.Net.NetworkCredential(mailAccount.Username, mailAccount.Password);
           server.EnableSsl = true;
       }
        public void SendEmail(MailMessage mail)
        {     
                server.Send(mail);      
           
        }
        public void Logout()
        {

          server.Dispose();
        }
       
    }
}
