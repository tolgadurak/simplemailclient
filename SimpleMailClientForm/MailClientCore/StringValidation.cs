﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleMailClientForm.MailClientCore
{
    class StringValidation
    {
        public static bool MathesRegexForEmail(String username)
        {
            Regex pattern = new Regex("\\w+@\\w+\\.{1}\\w+");          
            Match match = pattern.Match(username);        
            return match.Success;
        }
    }
}
