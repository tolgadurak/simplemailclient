﻿
namespace SimpleMailClientForm
{
   public partial class MailClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attachmentDialog = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.settingsLabel = new System.Windows.Forms.Label();
            this.accountLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.writeEmail = new System.Windows.Forms.Label();
            this.inboxLabel = new System.Windows.Forms.Label();
            this.outboxLabel = new System.Windows.Forms.Label();
            this.draftsLabel = new System.Windows.Forms.Label();
            this.trashLabel = new System.Windows.Forms.Label();
            this.emailImage = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emailImage)).BeginInit();
            this.SuspendLayout();
            // 
            // attachmentDialog
            // 
            this.attachmentDialog.FileName = "openFileDialog1";
            this.attachmentDialog.Title = "Dosya Seçin";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.75758F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.24242F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 475F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(825, 475);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.61585F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.38415F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(166, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(656, 469);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel5, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.emailImage, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.61621F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.90831F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(157, 469);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.settingsLabel);
            this.flowLayoutPanel5.Controls.Add(this.accountLabel);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 315);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(151, 151);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // settingsLabel
            // 
            this.settingsLabel.AutoSize = true;
            this.settingsLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsLabel.Location = new System.Drawing.Point(3, 128);
            this.settingsLabel.Name = "settingsLabel";
            this.settingsLabel.Size = new System.Drawing.Size(66, 23);
            this.settingsLabel.TabIndex = 2;
            this.settingsLabel.Text = "Ayarlar";
            this.settingsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.settingsLabel.MouseLeave += new System.EventHandler(this.settingsLabel_Leave);
            this.settingsLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.settingsLabel_Move);
            // 
            // accountLabel
            // 
            this.accountLabel.AutoSize = true;
            this.accountLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountLabel.Location = new System.Drawing.Point(3, 105);
            this.accountLabel.Name = "accountLabel";
            this.accountLabel.Size = new System.Drawing.Size(93, 23);
            this.accountLabel.TabIndex = 1;
            this.accountLabel.Text = "Hesap Ekle";
            this.accountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.accountLabel.Click += new System.EventHandler(this.accountLabel_Click);
            this.accountLabel.MouseLeave += new System.EventHandler(this.accountLabel_Leave);
            this.accountLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.accountLabel_Move);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelWelcome);
            this.flowLayoutPanel2.Controls.Add(this.writeEmail);
            this.flowLayoutPanel2.Controls.Add(this.inboxLabel);
            this.flowLayoutPanel2.Controls.Add(this.outboxLabel);
            this.flowLayoutPanel2.Controls.Add(this.draftsLabel);
            this.flowLayoutPanel2.Controls.Add(this.trashLabel);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 95);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(151, 214);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWelcome.Location = new System.Drawing.Point(3, 0);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(96, 23);
            this.labelWelcome.TabIndex = 5;
            this.labelWelcome.Text = "Giriş Ekranı";
            this.labelWelcome.Click += new System.EventHandler(this.labelWelcome_Click);
            this.labelWelcome.MouseLeave += new System.EventHandler(this.labelWelcome_MouseLeave);
            this.labelWelcome.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelWelcome_MouseMove);
            // 
            // writeEmail
            // 
            this.writeEmail.AutoSize = true;
            this.writeEmail.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.writeEmail.Location = new System.Drawing.Point(3, 23);
            this.writeEmail.Name = "writeEmail";
            this.writeEmail.Size = new System.Drawing.Size(97, 23);
            this.writeEmail.TabIndex = 4;
            this.writeEmail.Text = "E-Posta Yaz";
            this.writeEmail.Click += new System.EventHandler(this.writeEmail_Click);
            this.writeEmail.MouseLeave += new System.EventHandler(this.writeEmail_Leave);
            this.writeEmail.MouseMove += new System.Windows.Forms.MouseEventHandler(this.writeEmail_Move);
            // 
            // inboxLabel
            // 
            this.inboxLabel.AutoSize = true;
            this.inboxLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inboxLabel.Location = new System.Drawing.Point(3, 46);
            this.inboxLabel.Name = "inboxLabel";
            this.inboxLabel.Size = new System.Drawing.Size(112, 23);
            this.inboxLabel.TabIndex = 0;
            this.inboxLabel.Text = "Gelen Kutusu";
            this.inboxLabel.Click += new System.EventHandler(this.inboxLabel_Click);
            this.inboxLabel.MouseLeave += new System.EventHandler(this.inboxLabel_Leave);
            this.inboxLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.inboxLabel_Move);
            // 
            // outboxLabel
            // 
            this.outboxLabel.AutoSize = true;
            this.outboxLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outboxLabel.Location = new System.Drawing.Point(3, 69);
            this.outboxLabel.Name = "outboxLabel";
            this.outboxLabel.Size = new System.Drawing.Size(115, 23);
            this.outboxLabel.TabIndex = 1;
            this.outboxLabel.Text = "Gönderilenler";
            this.outboxLabel.Click += new System.EventHandler(this.outboxLabel_Click);
            this.outboxLabel.MouseLeave += new System.EventHandler(this.outboxLabel_Leave);
            this.outboxLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.outboxLabel_Move);
            // 
            // draftsLabel
            // 
            this.draftsLabel.AutoSize = true;
            this.draftsLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.draftsLabel.Location = new System.Drawing.Point(3, 92);
            this.draftsLabel.Name = "draftsLabel";
            this.draftsLabel.Size = new System.Drawing.Size(77, 23);
            this.draftsLabel.TabIndex = 3;
            this.draftsLabel.Text = "Taslaklar";
            this.draftsLabel.Click += new System.EventHandler(this.draftsLabel_Click);
            this.draftsLabel.MouseLeave += new System.EventHandler(this.spamLabel_Leave);
            this.draftsLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.spamLabel_Move);
            // 
            // trashLabel
            // 
            this.trashLabel.AutoSize = true;
            this.trashLabel.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trashLabel.Location = new System.Drawing.Point(3, 115);
            this.trashLabel.Name = "trashLabel";
            this.trashLabel.Size = new System.Drawing.Size(98, 23);
            this.trashLabel.TabIndex = 2;
            this.trashLabel.Text = "Çöp Kutusu";
            this.trashLabel.Click += new System.EventHandler(this.trashLabel_Click);
            this.trashLabel.MouseLeave += new System.EventHandler(this.trashLabel_Leave);
            this.trashLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.trashLabel_Move);
            // 
            // emailImage
            // 
            this.emailImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailImage.Image = global::SimpleMailClientForm.Properties.Resources.blank_mail;
            this.emailImage.Location = new System.Drawing.Point(3, 3);
            this.emailImage.Name = "emailImage";
            this.emailImage.Size = new System.Drawing.Size(151, 86);
            this.emailImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.emailImage.TabIndex = 5;
            this.emailImage.TabStop = false;
            // 
            // MailClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 475);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MailClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple Mail Client";
            this.Load += new System.EventHandler(this.MailClientForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emailImage)).EndInit();
            this.ResumeLayout(false);

        }

       

        

       

        #endregion

        private System.Windows.Forms.OpenFileDialog attachmentDialog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.Label writeEmail;
        private System.Windows.Forms.Label inboxLabel;
        private System.Windows.Forms.Label outboxLabel;
        private System.Windows.Forms.Label draftsLabel;
        private System.Windows.Forms.Label trashLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label settingsLabel;
        private System.Windows.Forms.Label accountLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox emailImage;
       
    }
}

