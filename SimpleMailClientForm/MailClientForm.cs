﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleMailClientForm.MailClientCore;
using System.Net.Mail;



namespace SimpleMailClientForm
{
    public partial class MailClientForm : Form
    {
        SmtpServer server;
        IMAPServer serverIMAP;
        AccountHolder mailAccount;
       // Attachment attachment;
        NewEmail newEmailForm;
        public static ListView inbox = new ListView();
        ListView sentbox = new ListView();
        ListView draftbox = new ListView();
        ListView trashbox = new ListView();
        RichTextBox bodyRichTextBox = new RichTextBox();
        WebBrowser webBrowser1 = new WebBrowser();
        SettingsForm settingsForm;
        public MailClientForm(IMAPServer serverIMAP, SmtpServer server, AccountHolder mailAccount)
        {
            this.server = server;
            this.serverIMAP = serverIMAP;
            this.mailAccount = mailAccount;
            if (mailAccount.IsAccountNull()) 
            {
                MessageBox.Show("Kullanıcı tanımlanmadı.");
            }
            InitializeComponent();
        }

       /* private void sendButtonEvent(object sender, EventArgs e)
        {
            if (mailAccount.IsAccountNull())
            {
                MessageBox.Show("Kullanıcı Tanımlanmadı");
                return;
            }
            if (!StringValidation.MathesRegexForEmail(receiverTextBox.Text))
            {
                MessageBox.Show("Alıcı adresinde geçersiz e-mail adresi tespit edildi");
                return;
            }

            if (attachment != null)
            {
                MailHandler.CreateNewMail(mailAccount.Username, receiverTextBox.Text, subjectTextBox.Text, bodyRichTextBox.Text);
                MailHandler.AddAttachment(attachment);
            }
            else
            {
                MailHandler.CreateNewMail(mailAccount.Username, receiverTextBox.Text, subjectTextBox.Text, bodyRichTextBox.Text);
            }
            server.SendEmail(MailHandler.Mail);

        }

        private void browseButtonEvent(object sender, EventArgs e)
        {
            if (attachmentDialog.ShowDialog() == DialogResult.OK)
            {
                attachment = new Attachment(attachmentDialog.FileName);
            }
        }*/

        private void emailImage_Click(object sender, EventArgs e)
        {
           if(emailImage.Name == "outlook")
            System.Diagnostics.Process.Start("https://outlook.com");
           else if(emailImage.Name == "gmail")
               System.Diagnostics.Process.Start("https://mail.google.com/");

        }

        private void writeEmail_Click(object sender, EventArgs e)
        {
            if (newEmailForm == null)
            {
                newEmailForm = new NewEmail();
            }
            newEmailForm.ShowDialog();
        }
        private void accountLabel_Click(object sender, System.EventArgs e)
        {
            if (settingsForm == null)
            {
                settingsForm = new SettingsForm(tableLayoutPanel4,
                    flowLayoutPanel5,emailImage, serverIMAP, server, mailAccount);           
            }       
                settingsForm.ShowDialog();           
        }

        private void outboxLabel_Click(object sender, EventArgs e)
        {
            if (mailAccount.IsAccountNull())
            {
                MessageBox.Show("Kullanıcı Tanımlanmadı");
                return;
            }
            sentbox.Items.Clear();           
            foreach (var v in serverIMAP.GetAllMessagesFromSpecifiedFolder(serverIMAP.Sent))
            {
                string[] row = { v.Subject, v.From.Address, v.Date.ToString() };
                ListViewItem item = new ListViewItem(row);
                sentbox.Items.Add(item);
            }
            this.tableLayoutPanel4.Controls.Clear();                                
          
                this.tableLayoutPanel4.Controls.Add(sentbox);
                             
            AutoResizeColumn(sentbox);
            sentbox.Refresh();
        }

        private void sentbox_Click(object sender, EventArgs e)
        {
            
            if (!tableLayoutPanel4.Controls.Contains(webBrowser1))
            {
                this.tableLayoutPanel4.Controls.Add(webBrowser1);
            }

            webBrowser1.DocumentText = serverIMAP.Sent.Messages.ElementAt(sentbox.SelectedIndices[0]).Body.Html;
            webBrowser1.Focus();
        }

        private void inboxLabel_Click(object sender, EventArgs e)
        {
            if (mailAccount.IsAccountNull())
            {
                MessageBox.Show("Kullanıcı Tanımlanmadı");
                return;
            }
            inbox.Items.Clear();
            foreach (var v in serverIMAP.GetAllMessagesFromSpecifiedFolder(serverIMAP.Inbox))
            {
                string[] row = { v.Subject, v.From.Address, v.Date.ToString() };
                ListViewItem item = new ListViewItem(row);
                inbox.Items.Add(item);
            }
            this.tableLayoutPanel4.Controls.Clear();
            

                this.tableLayoutPanel4.Controls.Add(inbox,0,0);
       
            AutoResizeColumn(inbox);  
            inbox.Refresh();
        }

        void inbox_Click(object sender, EventArgs e)
        {
           
            if (!tableLayoutPanel4.Controls.Contains(webBrowser1))
            {
                this.tableLayoutPanel4.Controls.Add(webBrowser1,2,0);
            
            }         
            webBrowser1.DocumentText = serverIMAP.Inbox.Messages.ElementAt(inbox.SelectedIndices[0]).Body.Html;
            webBrowser1.Focus();
        }

        private void draftsLabel_Click(object sender, EventArgs e)
        {
            if (mailAccount.IsAccountNull())
            {
                MessageBox.Show("Kullanıcı Tanımlanmadı");
                return;
            }
            draftbox.Items.Clear();
            foreach (var v in serverIMAP.GetAllMessagesFromSpecifiedFolder(serverIMAP.Drafts))
            {
                string[] row = { v.Subject, v.From.Address, v.Date.ToString() };
                ListViewItem item = new ListViewItem(row);
                draftbox.Items.Add(item);
            }
            this.tableLayoutPanel4.Controls.Clear();         
            this.tableLayoutPanel4.Controls.Add(draftbox);           
            AutoResizeColumn(draftbox);  
            draftbox.Refresh();
        }

        private void draftbox_Click(object sender, EventArgs e)
        {
           
            if (!tableLayoutPanel4.Controls.Contains(webBrowser1))
            {
                this.tableLayoutPanel4.Controls.Add(webBrowser1);
            }  
            webBrowser1.DocumentText = serverIMAP.Drafts.Messages.ElementAt(draftbox.SelectedIndices[0]).Body.Html;
            webBrowser1.Focus();
        }

        private void trashLabel_Click(object sender, EventArgs e)
        {
            if (mailAccount.IsAccountNull())
            {
                MessageBox.Show("Kullanıcı Tanımlanmadı");
                return;
            }
            trashbox.Items.Clear();
            foreach (var v in serverIMAP.GetAllMessagesFromSpecifiedFolder(serverIMAP.Trash))
            {
                string[] row = { v.Subject, v.From.Address, v.Date.ToString() };
                ListViewItem item = new ListViewItem(row);
                trashbox.Items.Add(item);
            }
            this.tableLayoutPanel4.Controls.Clear();        
            this.tableLayoutPanel4.Controls.Add(trashbox);                  
            AutoResizeColumn(trashbox);
            trashbox.Refresh();
        }

        private void trashbox_Click(object sender, EventArgs e)
        {
           
            if (!tableLayoutPanel4.Controls.Contains(webBrowser1))
            {
                this.tableLayoutPanel4.Controls.Add(webBrowser1);
            }            
            webBrowser1.DocumentText = serverIMAP.Trash.Messages.ElementAt(trashbox.SelectedIndices[0]).Body.Html;            
            webBrowser1.Focus();
        }

        private void MailClientForm_Load(object sender, EventArgs e)
        {
            bodyRichTextBox.Dock = DockStyle.Fill;
            webBrowser1.Dock = DockStyle.Fill;
            trashbox.Dock = DockStyle.Fill;
            trashbox.View = View.Details;
            draftbox.Dock = DockStyle.Fill;
            draftbox.View = View.Details;
            inbox.Dock = DockStyle.Fill;
            inbox.View = View.Details;
            sentbox.Dock = DockStyle.Fill;
            sentbox.View = View.Details;
            trashbox.Columns.Add("Mail Konusu");
            trashbox.Columns.Add("Gönderen");
            trashbox.Columns.Add("Gönderilme Tarihi");
            trashbox.Click += trashbox_Click;
            draftbox.Columns.Add("Mail Konusu");
            draftbox.Columns.Add("Gönderen");
            draftbox.Columns.Add("Gönderilme Tarihi");
            draftbox.Click += draftbox_Click;
            inbox.Columns.Add("Mail Konusu");
            inbox.Columns.Add("Gönderen");
            inbox.Columns.Add("Gönderilme Tarihi");
            inbox.Click += inbox_Click;
            sentbox.Columns.Add("Mail Konusu");
            sentbox.Columns.Add("Gönderen");
            sentbox.Columns.Add("Gönderilme Tarihi");
            sentbox.Click += sentbox_Click;
            sentbox.Visible = true;
            inbox.Visible = true;
            draftbox.Visible = true;
            trashbox.Visible = true;
            webBrowser1.Visible = true;
            bodyRichTextBox.Visible = true;
        }

        private void AutoResizeColumn(ListView control)
        {
            control.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
            control.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
            control.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        private void labelWelcome_Click(object sender, EventArgs e)
        {
            tableLayoutPanel4.Controls.Clear();
        }    
    }
}
