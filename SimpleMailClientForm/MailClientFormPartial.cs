﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleMailClientForm;
using System.Windows.Forms;
using System.Drawing;
namespace SimpleMailClientForm
{
    public partial class MailClientForm
    {
        private void writeEmail_Leave(object sender, System.EventArgs e)
        {
            writeEmail.ForeColor = Color.Black;
        }

        private void writeEmail_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            writeEmail.ForeColor = Color.DarkBlue;
        }

        private void inboxLabel_Leave(object sender, System.EventArgs e)
        {
            inboxLabel.ForeColor = Color.Black;
        }

        private void inboxLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            inboxLabel.ForeColor = Color.DarkBlue;
        }

        private void outboxLabel_Leave(object sender, System.EventArgs e)
        {
            outboxLabel.ForeColor = Color.Black;
        }

        private void outboxLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            outboxLabel.ForeColor = Color.DarkBlue;
        }

        private void trashLabel_Leave(object sender, System.EventArgs e)
        {
            trashLabel.ForeColor = Color.Black;
        }

        private void trashLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            trashLabel.ForeColor = Color.DarkBlue;
        }

        private void spamLabel_Leave(object sender, System.EventArgs e)
        {
            draftsLabel.ForeColor = Color.Black;
        }

        private void spamLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            draftsLabel.ForeColor = Color.DarkBlue;
        }

        private void accountLabel_Leave(object sender, System.EventArgs e)
        {
            accountLabel.ForeColor = Color.Black;
        }

        private void accountLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            accountLabel.ForeColor = Color.DarkBlue;
        }

        private void settingsLabel_Leave(object sender, System.EventArgs e)
        {
            settingsLabel.ForeColor = Color.Black;
        }

        private void settingsLabel_Move(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            settingsLabel.ForeColor = Color.DarkBlue;
        }

        private void labelWelcome_MouseMove(object sender, MouseEventArgs e)
        {
            labelWelcome.ForeColor = Color.DarkBlue;
        }

        private void labelWelcome_MouseLeave(object sender, EventArgs e)
        {
            labelWelcome.ForeColor = Color.Black;
        }
    }
}
