﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Exchange.WebServices.Data;
using SimpleMailClientForm.MailClientCore;

namespace SimpleMailClientForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AccountHolder mailAccount = new AccountHolder();
            IMAPServer ServerIMAP = new IMAPServer();
            SmtpServer ServerSMTP = new SmtpServer();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MailClientForm(ServerIMAP, ServerSMTP, mailAccount));
            
            
        }
        
    }
}
