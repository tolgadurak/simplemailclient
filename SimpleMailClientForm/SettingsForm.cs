﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleMailClientForm.MailClientCore;
namespace SimpleMailClientForm
{
    public partial class SettingsForm : Form
    {
        AccountHolder mailAccount;
        IMAPServer ImapServer;
        SmtpServer SmtpServer;
        PictureBox emailImage;
        FlowLayoutPanel layout;
        TableLayoutPanel tableLayout;
        Label logoutLabel = new Label();
        public SettingsForm(TableLayoutPanel tableLayout ,FlowLayoutPanel layout, PictureBox emailImage, IMAPServer imapServer, SmtpServer server, AccountHolder mailAccount)
        {
            this.mailAccount = mailAccount;
            this.ImapServer = imapServer;
            this.SmtpServer = server;
            this.emailImage = emailImage;
            this.layout = layout;
            this.tableLayout = tableLayout;
            InitializeComponent();

        }

        private void connectButton_Click(object sender, EventArgs e)
        {

            mailAccount.Username = emailTextBox.Text;
            mailAccount.Password = passTextBox.Text;
            mailAccount.InitializeServerSettings();
            ImapServer.InitializeServer(mailAccount);          
         SmtpServer.InitializeServer(mailAccount);
         if (!mailAccount.IsAccountNull())
         {
             logoutLabel.AutoSize = true;
             logoutLabel.Font = new System.Drawing.Font("Consolas", 15.75F,
                 System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
             logoutLabel.Text = "Çıkış yap";
             logoutLabel.TabIndex = 0;
             logoutLabel.MouseLeave += logoutLabel_MouseLeave;
             logoutLabel.MouseMove += logoutLabel_MouseMove;
             logoutLabel.Click += logoutLabel_Click;
             ToolTip tooltip = new ToolTip();
             tooltip.SetToolTip(logoutLabel, mailAccount.Username);
              layout.Controls.Add(logoutLabel);
              ImapServer.EnableAutoSync(ImapServer.Inbox);
             setImageInMainForm();
             this.Hide();
         }
        }

        void logoutLabel_Click(object sender, EventArgs e)
        {
            ImapServer.Logout();
            SmtpServer.Logout();
            mailAccount.Clear();
            layout.Controls.Remove(logoutLabel);
            tableLayout.Controls.Clear();
        }

        void logoutLabel_MouseMove(object sender, MouseEventArgs e)
        {
            logoutLabel.ForeColor = Color.DarkBlue;
        }

        void logoutLabel_MouseLeave(object sender, EventArgs e)
        {
            logoutLabel.ForeColor = Color.Black;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void setImageInMainForm()
        {
            if (mailAccount.IMAPServerName == AccountHolder.GmailIMAPServer)
            {
                emailImage.Image = global::SimpleMailClientForm.Properties.Resources.gmail;
                emailImage.Name = "gmail";
            }
            else if (mailAccount.IMAPServerName == AccountHolder.OutlookIMAPServer)
            {
                 emailImage.Image = global::SimpleMailClientForm.Properties.Resources.outlook_com;
                 emailImage.Name = "outlook";
            }
        }
    }
}
